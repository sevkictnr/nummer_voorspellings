
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtGirilenSayi: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var imgGirilenSayiIcon: UIImageView!
    @IBOutlet weak var txtTahminEdilenSayi: UITextField!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var imgTahminEdilenSayiIcon: UIImageView!
    @IBOutlet weak var imgYildiz1: UIImageView!
    @IBOutlet weak var imgYildiz2: UIImageView!
    @IBOutlet weak var imgYildiz3: UIImageView!
    @IBOutlet weak var imgYildiz4: UIImageView!
    @IBOutlet weak var imgYildiz5: UIImageView!
    @IBOutlet weak var lblSonuc: UILabel!
    
    var stars:[UIImageView] = [UIImageView]()
    var maxTryCount : Int = 5
    var userTryCount : Int = 0
    var correctNumber : Int = -1
    var oyunStatus : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stars = [imgYildiz1, imgYildiz2, imgYildiz3, imgYildiz4, imgYildiz5]
        
        imgGirilenSayiIcon.isHidden = true
        imgTahminEdilenSayiIcon.isHidden = true
        btnCheck.isEnabled = false
        txtTahminEdilenSayi.isEnabled = false
        txtGirilenSayi.isEnabled = true
        txtGirilenSayi.isSecureTextEntry = true
        lblSonuc.text = ""
    }

    @IBAction func btnSaveClick(_ sender: Any) {
        imgGirilenSayiIcon.isHidden = false
        if let t = Int(txtGirilenSayi.text!) {
            print("girilen sayi \(t)")
            correctNumber = t
            btnCheck.isEnabled = true
            btnSave.isEnabled = false
            txtTahminEdilenSayi.isEnabled = true
            txtGirilenSayi.isEnabled  = false
            imgGirilenSayiIcon.image = UIImage(named: "onay")
        } else {
            print("sayi textfield den alinamadi")
        }
    }
    
    @IBAction func btnCheckClick(_ sender: UIButton) {
        
        if oyunStatus == true || userTryCount > maxTryCount {
            return
        }
        
        if let girilenSayi = Int(txtTahminEdilenSayi.text!) {
            userTryCount += 1
            stars[userTryCount-1].image = UIImage.init(named: "border_star")
            imgTahminEdilenSayiIcon.isHidden = false
            lblSonuc.isHidden = false
            if girilenSayi > correctNumber {
                imgTahminEdilenSayiIcon.image = UIImage.init(named: "below")
                txtTahminEdilenSayi.backgroundColor = UIColor.red
                lblSonuc.text = "Try again"
                lblSonuc.backgroundColor = UIColor.green
                txtTahminEdilenSayi.endEditing(true)
            } else if girilenSayi < correctNumber {
                imgTahminEdilenSayiIcon.image = UIImage.init(named: "upward")
                txtTahminEdilenSayi.backgroundColor = UIColor.red
                lblSonuc.text = "Try again"
                lblSonuc.backgroundColor = UIColor.green
                txtTahminEdilenSayi.endEditing(true)
            } else {
                imgTahminEdilenSayiIcon.image = UIImage.init(named: "okay")
                txtTahminEdilenSayi.backgroundColor = UIColor.green
                oyunStatus = true
                lblSonuc.text = "YOU WIN !!!"
                lblSonuc.backgroundColor = UIColor.green
                txtTahminEdilenSayi.endEditing(true)
                
                let alertController = UIAlertController(title: "Succesful", message: "Congratulations ! You won", preferredStyle: UIAlertController.Style.alert)
                let newGame = UIAlertAction(title: "New Game", style: UIAlertAction.Style.default, handler: {
                    (action : UIAlertAction) in
                    print("tekrar oynamak icin girisimde bulunuldu")
                    self.reset()
                })
                let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
                
                alertController.addAction(newGame)
                alertController.addAction(okAction)
                
                present(alertController, animated: true, completion: nil)
                
                return
            }
        } else {
            imgTahminEdilenSayiIcon.image = UIImage.init(named: "hata")
            let alertController = UIAlertController(title: "Input Error", message: "wrong value entered", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
        
        if userTryCount == maxTryCount {
            btnCheck.isEnabled = false
            imgTahminEdilenSayiIcon.image = UIImage.init(named: "hata")
            lblSonuc.text = "Did not successfully   \(correctNumber) "
            txtTahminEdilenSayi.isSecureTextEntry = false
            txtTahminEdilenSayi.endEditing(true)
            imgTahminEdilenSayiIcon.image = UIImage.init(named: "hata")
            let alertController = UIAlertController(title: "You couldn't find the number", message: "You couldn't win the game.", preferredStyle: UIAlertController.Style.alert)
            let newGame = UIAlertAction(title: "New Game", style: UIAlertAction.Style.default, handler: {
                               (action : UIAlertAction) in
                               print("tekrar oynamak icin girisimde bulunuldu")
                self.reset()
                           })
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
            alertController.addAction(newGame)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
            return
        }
    }
    
    func reset() {
        maxTryCount = 5
         userTryCount = 0
         correctNumber = -1
         oyunStatus = false
        imgGirilenSayiIcon.isHidden = true
        imgTahminEdilenSayiIcon.isHidden = true
        btnCheck.isEnabled = false
        txtTahminEdilenSayi.backgroundColor = UIColor.white
        txtTahminEdilenSayi.isEnabled = false
        txtGirilenSayi.isEnabled = true
        btnSave.isEnabled = true
        txtGirilenSayi.text = ""
        txtTahminEdilenSayi.text = ""
        txtGirilenSayi.isSecureTextEntry = true
        lblSonuc.text = ""
        lblSonuc.isHidden = true
        for item in stars {
            item.image = UIImage.init(named: "yellow_star")
        }
    }
}

